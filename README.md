Bio-Formats library patched for Icy.
The patch consist of:
- replace Apache Commons IO library by a newer version.
- replace org.json library with the custom version embedded into Micro-Manager to make it work properly. 