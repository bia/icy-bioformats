<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <artifactId>pom-icy</artifactId>
        <groupId>org.bioimageanalysis.icy</groupId>
        <version>2.1.1</version>
    </parent>

    <artifactId>icy-bioformats</artifactId>
    <version>6.11.0</version>
    <packaging>jar</packaging>

    <name>Icy BioFormats library</name>
    <description>BioFormats library patched to include custom version of Apache commons IO and JSON libraries</description>

    <licenses>
        <license>
            <name>GNU GPLv3</name>
            <url>https://www.gnu.org/licenses/gpl-3.0.en.html</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <build>
        <directory>build</directory>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>${project.artifactId}-fetch-httpcore</id>
                        <!-- <phase>generate-sources</phase> -->
                        <goals>
                            <goal>unpack-dependencies</goal>
                        </goals>
                        <configuration>
                            <includeArtifactIds>httpcore</includeArtifactIds>
                            <outputDirectory>${project.build.outputDirectory}</outputDirectory>
                            <stripVersion>true</stripVersion>
                            <excludeTransitive>true</excludeTransitive>
                        </configuration>
                    </execution>
                    <execution>
                        <id>${project.artifactId}-fetch-httpclient</id>
                        <!-- <phase>generate-sources</phase> -->
                        <goals>
                            <goal>unpack-dependencies</goal>
                        </goals>
                        <configuration>
                            <includeArtifactIds>httpclient</includeArtifactIds>
                            <outputDirectory>${project.build.outputDirectory}</outputDirectory>
                            <stripVersion>true</stripVersion>
                            <excludeTransitive>true</excludeTransitive>
                        </configuration>
                    </execution>
                    <execution>
                        <id>${project.artifactId}-fetch-commons-io</id>
                        <!-- <phase>generate-sources</phase> -->
                        <goals>
                            <goal>unpack-dependencies</goal>
                        </goals>
                        <configuration>
                            <includeArtifactIds>commons-io</includeArtifactIds>
                            <outputDirectory>${project.build.outputDirectory}</outputDirectory>
                            <stripVersion>true</stripVersion>
                            <excludeTransitive>true</excludeTransitive>
                        </configuration>
                    </execution>
                    <execution>
                        <id>${project.artifactId}-fetch-bioformats</id>
                        <!-- <phase>generate-sources</phase> -->
                        <goals>
                            <goal>unpack-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.outputDirectory}</outputDirectory>
                            <stripVersion>true</stripVersion>
                            <excludeTransitive>true</excludeTransitive>
                            <includeArtifactIds>bioformats_package</includeArtifactIds>
                            <excludes>org/apache/commons/io/**,org/apache/http/**,org/json/**</excludes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                        </manifest>
                        <addMavenDescriptor>false</addMavenDescriptor>
                    </archive>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <excludes>
                        <exclude>loci/formats/in/NativeND2Reader.java</exclude>
                    </excludes>
                </configuration>
            </plugin>

        </plugins>
    </build>

    <!-- to avoid version conflict -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.apache.httpcomponents</groupId>
                <artifactId>httpclient</artifactId>
                <version>${apache-httpclient.version}</version>
            </dependency>
            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>29.0-jre</version>
            </dependency>
            <dependency>
                <groupId>joda-time</groupId>
                <artifactId>joda-time</artifactId>
                <version>2.8.1</version>
            </dependency>
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-annotations</artifactId>
                <version>2.9.8</version>
            </dependency>
            <dependency>
                <groupId>xml-apis</groupId>
                <artifactId>xml-apis</artifactId>
                <version>1.3.04</version>
            </dependency>
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>1.7.22</version>
            </dependency>
            <dependency>
                <groupId>commons-logging</groupId>
                <artifactId>commons-logging</artifactId>
                <version>${apache-commons-logging.version}</version>
            </dependency>
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-databind</artifactId>
                <version>2.9.8</version>
            </dependency>
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-core</artifactId>
                <version>2.9.8</version>
            </dependency>
        </dependencies>
    </dependencyManagement>


    <dependencies>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpcore</artifactId>
            <version>4.4.15</version>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.11.0</version>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>ome</groupId>
            <artifactId>bioformats_package</artifactId>
            <version>6.11.0</version>
            <optional>true</optional>
        </dependency>
    </dependencies>


    <!-- Icy Maven repository (to find parent POM, OME repo already here) -->
    <repositories>
        <repository>
            <id>ome-repo</id>
            <name>OME repository</name>
            <url>https://artifacts.openmicroscopy.org/artifactory/ome.releases/</url>
        </repository>
        <repository>
            <id>icy</id>
            <name>Icy's Nexus</name>
            <url>https://icy-nexus.pasteur.fr/repository/Icy/</url>
        </repository>
    </repositories>


</project>